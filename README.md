# Informações sobre o Curso

Olá Alunos :)

Este curso possui o objetivo de contemplarmos os conceitos básciso das principais tecnologias usadas para o desenvolvimento web moderno.

Além disso iremos trabalhar com tecnologias open-source, a fim da inserção do aluno dentro desse universo.

Dentro dele, iremos cobrir as seguintes tecnologias:
- **Git**: é um sistema de controle de versões distribuído, usado principalmente no desenvolvimento de software, mas pode ser usado para registrar o histórico de edições de qualquer tipo.
- **Javascript**: é uma linguagem de programação interpretada estruturada, de script em alto nível com tipagem dinâmica fraca e multiparadigma. Juntamente com HTML e CSS, o JavaScript é uma das três principais tecnologias da World Wide Web.
- **React**: Biblioteca para construção de Interfaces de Usuário (UI) criada pelo Facebook.
- **TypeScrtip**: TypeScript é um superconjunto de JavaScript desenvolvido pela Microsoft que adiciona tipagem e alguns outros recursos a linguagem.
- **NodeJS** - Ambiente de desenvolvimento backend para construções de APIs modernas usando a linguagem JavaScript.
- **MongoDB** - Banco de dados NOSQL escalável usado para persistência de dados.

## Intrutores

- Gabriel: gabriel.carvalho@acct.global
- Rodrigo: rodrigo.alves@acct.global

## Slack
Disponibilizaremos um canal de comunicação online que poderá ser utilizado pelos alunos para **tirar dúvidas em qualquer dia e horário da semana**. A resposta será disponibiliza em até 1 dia útil.

Dentro do Slack teremos os canais por tema (#react, #git, #javascript, #nodejs, #mongodb) e o canal de anúncios gerais (#general).

Sempre que disponibilizarmos uma nova aula, faremos o anúncio notificando no canal #general do Slack.

**Link de Acesso**: [https://join.slack.com/t/acctlab/shared_invite/zt-7ch8n1sy-FlCOrJkbGoVGPvBcotdpWw](https://join.slack.com/t/acctlab/shared_invite/zt-7ch8n1sy-FlCOrJkbGoVGPvBcotdpWw)

![Slack Link](https://i.imgur.com/8udnNqJ.png)

## Programação do Curso

### Carga horária:
- Duração total do curso de 4 meses - Haverão 2 turmas por ano.
- 2 horas aulas online semanais - Horário 17h às 19h, segundas-feiras.

### Primeira fase (40 horas) - FRONT-END:

#### Construção de aplicações web front-end com React UI, com ênfase em Progressive Web Apps (Google). - 10 horas
- Semana 1 - Introdução ao Javascript e ao GIT
- Semana 2 - Aplicações mais avançadas em Javascript e GIT

#### Construção de aplicações web front-end com React UI, com ênfase em Progressive Web Apps (Google). - 30 horas
- Semana 3 - Introdução ao React
- Semana 4 - Uso de Components and Props, State and Lifecycle, Handling Events,  Conditional Rendering.
- Semana 5 - Lists and Keys, Forms, Lifting State Up, Composition vs Inheritance
- Semana 6 - Conceitos de PWA, Networking, Requisições e `fetch`
- Semana 7 - Hooks 
- Semana 8 - Typescript React

### Segunda fase (40 horas) - BACK-END:

#### Construções de aplicações web back-end e API's com NODEJS e Persistência de dados em banco de dados NOSQL com MongoDB - 25 horas
- Semana 9 - Introdução ao Node.js
- Semana 10 - Criação de APIs com Express
- Semana 11 - Migrando nossa API para Typescript 
- Semana 12 - Headers e Cabeçalhos HTTP
- Semana 13 - Introdução ao MongoDB
- Semana 14 - Utilizando APIs consumindo o banco de dados

#### Projeto Final - 15 horas
- Semana 15 - Entrega do Back-end e checkpoint do projeto
- Semana 16 - Entrega do Projeto (Back-end e Front-end)
